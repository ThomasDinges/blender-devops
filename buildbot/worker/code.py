#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import pathlib
import sys

from collections import OrderedDict
from typing import Callable

sys.path.append(str(pathlib.Path(__file__).resolve().parent.parent))

import worker.configure
import worker.utils

import worker.blender.update
import worker.blender.lint
import worker.blender.compile
import worker.blender.test
import worker.blender.sign
import worker.blender.pack


if __name__ == "__main__":
    steps: worker.utils.BuilderSteps = OrderedDict()
    steps["configure-machine"] = worker.configure.configure_machine
    steps["update-code"] = worker.blender.update.update
    steps["lint-code"] = worker.blender.lint.lint
    steps["compile-code"] = worker.blender.compile.compile_code
    steps["compile-gpu"] = worker.blender.compile.compile_gpu
    steps["compile-install"] = worker.blender.compile.compile_install
    steps["test-code"] = worker.blender.test.test
    steps["sign-code-binaries"] = worker.blender.sign.sign
    steps["package-code-binaries"] = worker.blender.pack.pack
    steps["clean"] = worker.blender.CodeBuilder.clean

    parser = worker.blender.create_argument_parser(steps=steps)

    args = parser.parse_args()
    builder = worker.blender.CodeBuilder(args)
    builder.setup_track_path()
    builder.run(args.step, steps)
