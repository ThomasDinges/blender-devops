# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import pathlib
import sys

sys.path.append(str(pathlib.Path(__file__).resolve().parent.parent.parent))

import worker.blender.sign
import worker.utils

path = pathlib.Path(sys.argv[1]).resolve()

worker.blender.sign.sign_windows("PROD", path)

if str(path).find("Unspecified") != -1:
    print("Probably running with cpack command, adding Blender path")
    blender_path = path.parent / "Blender"
    worker.blender.sign.sign_windows("PROD", blender_path)

print("Codesign for cpack is finished")

# Only do this for zip
if str(path).find("ZIP") != -1:
    new_path = path.parent / path.name.replace("-windows64", "")
    package_file_path = new_path.parent / (new_path.name + ".zip")

    worker.utils.call(["7z", "a", "-tzip", package_file_path, path, "-r"])
    worker.utils.call(["7z", "rn", package_file_path, path.name, new_path.name])
