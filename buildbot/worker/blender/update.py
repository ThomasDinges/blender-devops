# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import os
import pathlib
import sys

from typing import List

import worker.blender
import worker.utils


def _clean_folders(builder: worker.blender.CodeBuilder) -> None:
    # Delete build folders.
    delete_paths: List[pathlib.Path] = []
    if builder.needs_full_clean:
        delete_paths += [builder.build_dir]
    else:
        delete_paths += [builder.build_dir / "Testing"]
        delete_paths += [builder.build_dir / "bin" / "tests"]

    # Delete install and packaging folders
    delete_paths += [builder.install_dir]
    delete_paths += [builder.package_dir]

    # Debug, install and package files from other tracks
    branches_config = builder.get_branches_config()
    other_tracks = branches_config.track_major_minor_versions.keys()
    for other_track in other_tracks:
        other_track_path = builder.tracks_root_path / ("blender-" + other_track)
        # TODO: don't hardcode these folder names
        delete_paths += [other_track_path / "build_download"]
        delete_paths += [other_track_path / "build_linux"]
        delete_paths += [other_track_path / "build_darwin"]
        delete_paths += [other_track_path / "build_package"]
        delete_paths += [other_track_path / "build_source"]
        delete_paths += [other_track_path / "build_debug"]
        delete_paths += [other_track_path / "build_arm64_debug"]
        delete_paths += [other_track_path / "build_x86_64_debug"]
        delete_paths += [other_track_path / "build_sanitizer"]
        delete_paths += [other_track_path / "build_arm64_sanitizer"]
        delete_paths += [other_track_path / "build_x86_64_sanitizer"]
        delete_paths += [other_track_path / "install_release"]
        delete_paths += [other_track_path / "install_asserts"]
        delete_paths += [other_track_path / "install_sanitizer"]
        delete_paths += [other_track_path / "install_debug"]

    for delete_path in delete_paths:
        # Retry several times, giving it a chance for possible antivirus to release a lock
        # on MSI files within the build folder.
        worker.utils.remove_dir(delete_path, retry_count=5, retry_wait_time=5)

    # Might be left over from git command hanging
    stack_dump_file_path = builder.code_path / "sh.exe.stackdump"
    worker.utils.remove_file(stack_dump_file_path)


def _svn_info_and_revert(builder: worker.blender.CodeBuilder) -> None:
    lib_path = builder.track_path / "lib"

    if lib_path.exists():
        for lib in lib_path.iterdir():
            if lib.is_dir() and not lib.name.startswith("."):
                lib = lib.resolve()
                worker.utils.call(["svn", "info", lib])
                worker.utils.call(["svn", "status", lib])
                worker.utils.call(["svn", "revert", "-R", lib])


def update(builder: worker.blender.CodeBuilder) -> None:
    _clean_folders(builder)

    builder.update_source()
    os.chdir(builder.code_path)

    # Run make update
    cmd = [
        sys.executable,
        builder.code_path / "build_files" / "utils" / "make_update.py",
        "--no-blender",
        "--use-linux-libraries",
        "--use-tests",
        "--architecture",
        builder.architecture,
    ]

    _svn_info_and_revert(builder)
    worker.utils.call(cmd)
    _svn_info_and_revert(builder)
