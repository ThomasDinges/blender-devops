# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import importlib

from typing import Any


def get_config(devops_env_id: str) -> Any:
    if devops_env_id == "LOCAL":
        import conf.local.worker

        importlib.reload(conf.local.worker)
        return conf.local.worker
    else:
        import conf.production.worker

        importlib.reload(conf.production.worker)
        return conf.production.worker


# Maybe useful in the future.
#
# import pathlib
# import importlib.util
#
# def _load_module_config(path: pathlib.Path) -> Any:
#     filepath = pathlib.Path(__file__).parent / path
#     spec = importlib.util.spec_from_file_location("config_module", filepath)
#     if not spec:
#         raise BaseException("Failed to load config module spec")
#     config_module = importlib.util.module_from_spec(spec)
#     if not spec.loader:
#         raise BaseException("Failed to load config module spec loader")
#     spec.loader.exec_module(config_module)
#     return config_module
