# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import os
import pathlib

from typing import Optional, Tuple

# Where tracks data is stored.
tracks_root_path = pathlib.Path.home() / "git"

# Software cache
software_cache_path = tracks_root_path / "downloads" / "software" / "workers"

# Docs delivery.
docs_user = os.getlogin()
docs_machine = "127.0.0.1"
docs_folder = tracks_root_path / "delivery" / "docs"
docs_port = 22

# Studio docs delivery.
studio_user = os.getlogin()
studio_machine = "127.0.0.1"
studio_folder = tracks_root_path / "delivery" / "studio" / "blender-studio-pipeline"
studio_port = 22

# Download delivery.
download_user = os.getlogin()
download_machine = "127.0.0.1"
download_source_folder = tracks_root_path / "delivery" / "download" / "source"
download_release_folder = tracks_root_path / "delivery" / "download" / "release"
download_port = 22

# Buildbot download delivery
buildbot_download_folder = tracks_root_path / "delivery" / "buildbot"

# Code signing
sign_code_windows_certificate = None  # "Blender Self Code Sign SPC"
sign_code_windows_time_servers = ["http://ts.ssl.com"]
sign_code_windows_server_url = "http://fake-windows-sign-server"

sign_code_darwin_certificate = None
sign_code_darwin_team_id = None
sign_code_darwin_apple_id = None
sign_code_darwin_keychain_profile = None


def darwin_keychain_password(service_env_id: str) -> str:
    return "fake_keychain_password"


# Steam
steam_app_id = None
steam_platform_depot_ids = {
    "windows": None,
    "linux": None,
    "darwin": None,
}


def steam_credentials(service_env_id: str) -> Tuple[str, str]:
    return "fake_steam_username", "fake_steam_password"


# Snap
def snap_credentials(service_env_id: str) -> str:
    return "fake_snap_credentials"


# Windows Store
windows_store_self_sign = False


def windows_store_certificate(service_env_id: str) -> str:
    # return sign_code_windows_certificate
    return "fake_windows_store_publisher"


# PyPI
def pypi_token(service_env_id: str) -> str:
    return "fake_pypi_token"


# Gitea
def gitea_api_token(service_env_id: str) -> Optional[str]:
    return None
