# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

import importlib


def _get_config(devops_env_id: str):
    if devops_env_id == "LOCAL":
        import conf.local.machines

        importlib.reload(conf.local.machines)
        return conf.local.machines
    else:
        import conf.production.machines

        importlib.reload(conf.production.machines)
        return conf.production.machines


def fetch_platform_worker_names(devops_env_id: str):
    machines_config = _get_config(devops_env_id)
    return machines_config.get_worker_names(devops_env_id)


def get_worker_password(devops_env_id: str, worker_name: str) -> str:
    machines_config = _get_config(devops_env_id)
    return machines_config.get_worker_password(worker_name)


def fetch_local_worker_names():
    worker_names = []
    worker_numbers = range(1, 5, 1)
    for worker_number in worker_numbers:
        worker_id = str(worker_number).zfill(2)
        worker_name = f"local-coordinator-{worker_id}"
        worker_names += [worker_name]

    return worker_names
