# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: 2011-2024 Blender Authors
# <pep8 compliant>

# Builders for deploying Blender releases.

import buildbot.plugins

import conf.branches
import pipeline.common


def populate(devops_env_id):
    properties = [
        buildbot.plugins.util.BooleanParameter(
            name="needs_full_clean",
            label="Full clean -> removes build workspace on machine",
            required=True,
            strict=True,
            default=False,
        ),
    ]

    return pipeline.common.create_pipeline(
        devops_env_id,
        "code-artifacts-deploy",
        "code_deploy.py",
        [
            "configure-machine",
            "update-code",
            "package-source",
            "package-source-complete",
            "pull-artifacts",
            "repackage-artifacts",
            "deploy-artifacts",
            "monitor-artifacts",
            "clean",
        ],
        conf.branches.code_deploy_track_ids,
        properties,
        "blender.git",
        ["linux-x86_64-general"],
    )
